const toggle = document.querySelector('.toggle');
const panel = document.querySelector('.panel');

toggle.addEventListener('change', () => {
  panel.classList.toggle('show-monthly');
})
